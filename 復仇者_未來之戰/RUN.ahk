﻿#z::Pause,Toggle
Coordmode, pixel, screen

;20160410 zoearth 是否有圖片.尋找次數
haveImg(_img,wSec:=1)
{
	Loop,%wSec%
	{
		ImageSearch, FoundX1, FoundY1, 0,0, A_ScreenWidth, A_ScreenHeight, *20 *Trans0x000000 %_img%
		if (ErrorLevel = 2)
		{
			MsgBox "Could not conduct the search."
			Pause
			return 0
		}
		if (FoundX1 > 0 && FoundY1 > 0 )
		{
			break
		}
		Sleep, 100
	}
	if (FoundX1 > 0 && FoundY1 > 0 )
	{
		return 1
	}
	return 0
}

clickImg(_img,_addX:=0,_addY:=0)
{
	ImageSearch, FoundX1, FoundY1, 0,0, A_ScreenWidth, A_ScreenHeight, *20 *Trans0x000000 %_img%
	if (ErrorLevel = 2)
	{
		return false
	}

	if (FoundX1 > 0 && FoundY1 > 0 )
	{
		FoundX1 := FoundX1 + 10 + _addX
		FoundY1 := FoundY1 + 10 + _addY
		Click %FoundX1%, %FoundY1%
		return true
	}
	return false
}

#s::
	_dayM = 1
    
    ;鋼鐵人
	_NC1  = 1
    ;蜘蛛人
	_NC2  = 1
    ;美國隊長
	_NC3  = 1
    ;奧創1-1
    _NC4  = 1
    ;奧創1-2
    _NC5  = 1
	Loop
	{
		; 結束
		if (haveImg("能量不足.png",2))
		{
            ; 取得免費能量
            if (haveImg("能量不足_取消.png",10))
            {
                clickImg("能量不足_取消.png")
                if (haveImg("體力_補充加.png",10))
                {
                    clickImg("體力_補充加.png")
                    if (haveImg("能量_沒有免費的.png",10))
                    {
                        ;關閉程式 與 關機
                        if (haveImg("關閉X.png",10))
                        {
                            clickImg("關閉X.png")
                            if (haveImg("關閉C.png",10))
                            {
                                if (clickImg("關閉C.png"))
                                {
                                    if (A_Hour > 22 || A_Hour < 7)
                                    {
                                        Sleep, 30000
                                        ;MsgBox "關機"
                                        Shutdown 1
                                    }
                                    Pause
                                }
                            }
                        }
                        Sleep, 1000000
                    }
                    else if (haveImg("免費_能量的免費.png",10))
                    {
                        clickImg("免費_能量的免費.png")
                        if (haveImg("體力補充_購買的購.png",10))
                        {
                            clickImg("體力補充_購買的購.png")
                        }
                    }
                }
            }
		}
		else if (haveImg("每日_可用次數為0.png"))
		{
			_dayM = 0
		}
		else if (haveImg("跳過.png"))
		{
			clickImg("跳過.png")
		}
		else if (haveImg("通用_取消.png",2))
		{
			clickImg("通用_取消.png")
		}
		else if (haveImg("通用_確定.png",2))
		{
			clickImg("通用_確定.png")
		}
		else if (haveImg("確定2.png",2))
		{
			clickImg("確定2.png")
		}
		else if (haveImg("確定3.png",2))
		{
			clickImg("確定3.png")
		}
		else if (haveImg("確定4.png",2))
		{
			clickImg("確定4.png")
		}
		else if (haveImg("關閉.png"))
		{
			clickImg("關閉.png")
		}
		else if (haveImg("關閉的關.png"))
		{
			clickImg("關閉的關.png")
		}
		else if (haveImg("關2.png"))
		{
			clickImg("關2.png")
		}
		else if (haveImg("關3.png"))
		{
			clickImg("關3.png")
		}
		;中斷每日10級任務
		else if (haveImg("每日.png",2) && haveImg("等級10_3星.png",2))
		{
			clickImg("切換任務_下.png")
		}
		;中斷任務
		else if (haveImg("劇情任務_生體認證次數0.png"))
		{
			if (haveImg("返回.png",5))
			{
				clickImg("返回.png")
			}
		}
		else if (haveImg("每日_次數0.png"))
		{
			_dayM = 0
			if (haveImg("返回.png",5))
			{
				clickImg("返回.png")
			}
		}
		else if (haveImg("重複任務.png"))
		{
			clickImg("重複任務.png")
		}
		else if (haveImg("任務開始.png",3))
		{
            if (haveImg("請選擇一位朋友.png",3))
            {
                if (haveImg("戰友_問號.png",3))
                {
                    clickImg("戰友_問號.png",0,100)
                }
            }
            clickImg("任務開始.png")
		}
		else if (_dayM == 1 && haveImg("每日_TAB.png",3))
		{
			clickImg("每日_TAB.png")
			if (haveImg("每日_關卡1.png",10))
			{
				clickImg("每日_關卡1.png")
			}
			else if (haveImg("每日_關卡2.png",10))
			{
				clickImg("每日_關卡2.png")
			}
		}
		else if (haveImg("劇情_TAB.png"))
		{
			clickImg("劇情_TAB.png")
		}
		; 劇情任務
		else if (haveImg("劇情任務鎖.png",2))
		{
			if (haveImg("劇情任務_上一頁.png"))
			{
				clickImg("劇情任務_上一頁.png")
			}
		}
		else if (_NC4 == 1 && haveImg("劇情任務_奧創_1_1.png"))
		{
            clickImg("劇情任務_奧創_1_1.png")
            _NC4 = 0
		}
		else if (_NC5 == 1 && haveImg("劇情任務_奧創_1_2.png"))
		{
            clickImg("劇情任務_奧創_1_2.png")
            _NC5 = 0
		}
		else if (_NC4 == 0 && _NC5 == 0 && _NC1 == 1 && haveImg("劇情任務_鋼鐵人.png"))
		{
			clickImg("劇情任務_鋼鐵人.png")
			_NC1 = 0
		}
		else if (_NC4 == 0 && _NC5 == 0 && _NC2 == 1 && haveImg("劇情任務_蜘蛛人.png"))
		{
			clickImg("劇情任務_蜘蛛人.png")
			_NC2 = 0
		}
		else if (_NC4 == 0 && _NC5 == 0 && _NC3 == 1 && haveImg("劇情任務_美國隊長.png"))
		{
			clickImg("劇情任務_美國隊長.png")
			_NC3 = 0
		}
		else if (_NC1 == 0 && _NC2 == 0 && _NC3 == 0 && _NC4 == 0 && _NC5 == 0 && haveImg("劇情任務列表生體認證5.png",3))
		{
			clickImg("劇情任務列表生體認證5.png")
		}
		else if (haveImg("劇情任務_上一頁.png"))
		{
			clickImg("劇情任務_上一頁.png")
		}
        ;戰鬥用
        else if (haveImg("關卡_組隊的組.png") )
        {
            Sleep, 10000
            clickImg("關卡_組隊的組.png",0,-100)
        }
		else if (haveImg("起始_開始任務.png"))
		{
			clickImg("起始_開始任務.png")
		}
		else if (haveImg("離開.png"))
		{
            if (haveImg("重複任務.png",3) != 1 )
            {
                clickImg("離開.png")
            }
		}
		else if (haveImg("返回.png"))
		{
			clickImg("返回.png")
		}
		Sleep, 600
	}