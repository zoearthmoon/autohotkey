﻿#z::Pause,Toggle
Coordmode, pixel, screen
haveImg(_img)
{
	ImageSearch, FoundX1, FoundY1, 0,0, A_ScreenWidth, A_ScreenHeight, *25 *Trans0xFFFFFF %_img%
	if (ErrorLevel = 2)
	{
		MsgBox "Could not conduct the search."
		Pause
		return false
	}

	if (FoundX1 > 0 && FoundY1 > 0 )
	{
		return true
	}
	return false
}

clickImg(_img)
{
	ImageSearch, FoundX1, FoundY1, 0,0, A_ScreenWidth, A_ScreenHeight, *25 *Trans0xFFFFFF %_img%
	if (ErrorLevel = 2)
	{
		MsgBox "Could not conduct the search."
		Pause
		return false
	}

	if (FoundX1 > 0 && FoundY1 > 0 )
	{
		FoundX1 := FoundX1 + 10
		FoundY1 := FoundY1 + 10
		Click %FoundX1%, %FoundY1%
		return true
	}
	return false
}

#s::
	Loop
	{
		; MsgBox % haveImg("test_TO.bmp")
		if (haveImg("test_TO.bmp") = true )
		{
			clickImg("test_TO.bmp")
		}
		Sleep, 1000
	}